"""askmebushe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="main_page"),
    path('hot_questions/', views.hot_questions, name="hot_questions"),
    path('login/', views.login, name="login_page"),
    path('signup/', views.signup, name="signup_page"),
    path('logout/', views.logout, name="logout"),
    path('ask/', views.ask, name="ask_page"),
    path('profile/edit/', views.settings, name="settings_page"),
    path('question/<int:num>', views.one_question, name="question_page"),
    path('tag/<str:tag_name>', views.tag_search, name="tag_page"),
    path('vote/', views.vote, name="vote")
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)