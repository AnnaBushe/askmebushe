# AskMeBushe

### Clone

Clone repository

```
git clone https://gitlab.com/AnnaBushe/askmebushe.git [<folder_name>]
```

### Virtual environment

Set up virtual environment

```
python3 -m venv venv
source venv/bin/activate
```

### Requirenments

Install requirements

```
pip3 install -r requirements.txt
```

### Database set up:

1) migrations

```
python3 manage.py migrate
```

2) fill database

```
python3 manage.py filldb
```

### Run

```
python3 manage.py runserver
