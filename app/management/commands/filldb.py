from django.core.management.base import BaseCommand
from app.models import *
from django.db.models import F
from random import choice, randint, sample, getrandbits
from faker import Faker

fake = Faker()


class Command(BaseCommand):
    @staticmethod
    def populate_user(records_num):
        users = []
        usernames = dict()
        for i in range(records_num):
            username = fake.simple_profile()['username']
            while username in usernames:
                username=fake.simple_profile()['username']
            users.append(User(username=username,
                              first_name=fake.first_name(),
                              last_name=fake.last_name(),
                              email=fake.email(),
                              password=fake.password()))

        User.objects.bulk_create(users)

    @staticmethod
    def populate_profile(records_num):
        users = []
        profiles = []
        usernames = set()
        for i in range(1, records_num+1):
            username = fake.simple_profile()['username']
            while username in usernames:
                username=fake.simple_profile()['username']
            usernames.add(username)
            user = User(username=username,
                      first_name=fake.first_name(),
                      last_name=fake.last_name(),
                      email=fake.email(),
                      password=fake.password())
            users.append(user)
            profiles.append(Profile(user_id=i))

        User.objects.bulk_create(users)
        Profile.objects.bulk_create(profiles)

    @staticmethod
    def populate_tag(records_num):
        tags = []
        names = set()
        for i in range(records_num):
            name = fake.word()
            while name in names:
                name = name + '_' + fake.word()
            names.add(name)

            tags.append(Tag(name=name))

        Tag.objects.bulk_create(tags)

    @staticmethod
    def populate_question(records_num):
        questions = []
        users_id = Profile.objects.all().values_list('id', flat=True)
        for i in range(records_num):
            questions.append(Question(title=fake.sentence(),
                                      text=fake.text(),
                                      date=fake.date(),
                                      user_id=users_id[randint(1, len(users_id)-1)],
                                      rating=0
                                      ))
        Question.objects.bulk_create(questions)

        questions = Question.objects.all()
        tags_id = list(Tag.objects.all().values_list('id', flat=True))
        for question in questions:
            question.tags.add(*sample(tags_id, randint(1, 7)))

    @staticmethod
    def populate_answer(records_num):
        answers = []
        users_id = Profile.objects.all().values_list('id', flat=True)
        question_id = Question.objects.all().values_list('id', flat=True)

        for i in range(records_num):
            answers.append(Answer(text=fake.text(),
                                  date=fake.date(),
                                  question_id=question_id[randint(1, len(question_id) - 1)],
                                  user_id=users_id[randint(1, len(users_id)-1)],
                                  correct=bool(getrandbits(1)),
                                  rating=0
                                  ))
        Answer.objects.bulk_create(answers)

    @staticmethod
    def populate_like_on_question(records_num):
        likes = []
        users = list(Profile.objects.all())
        questions = Question.objects.all()
        for question in questions:
            sample_users = sample(users, randint(0, len(users)))
            for user in sample_users:
                likes.append(LikeOnQuestion(attitude=choice([-1, 1]),
                                            question_id=question.id,
                                            user_id=user.id))
            if len(likes) > records_num:
                break
        LikeOnQuestion.objects.bulk_create(likes)

        questions = Question.objects.all()
        altered_questions = []
        for question in questions:
            likes = LikeOnQuestion.objects.filter(question=question.id)
            question.rating = sum(likes.values_list('attitude', flat=True))
            altered_questions.append(question)

        Question.objects.bulk_update(altered_questions, ['rating'])

    @staticmethod
    def populate_like_on_answer(records_num):
        likes = []
        users = list(Profile.objects.all())
        answers = Answer.objects.all()
        for answer in answers:
            sample_users = sample(users, randint(0, len(users)))
            for user in sample_users:
                likes.append(LikeOnAnswer(attitude=choice([-1, 1]),
                                          answer_id = answer.id,
                                          user_id=user.id))
            if len(likes) > records_num:
                break
        LikeOnAnswer.objects.bulk_create(likes)

        answers = Answer.objects.all()
        altered_answers = []
        for answer in answers:
            likes = LikeOnAnswer.objects.filter(answer=answer.id)
            answer.rating = sum(likes.values_list('attitude', flat=True))
            altered_answers.append(answer)

        Answer.objects.bulk_update(altered_answers, ['rating'])

    def handle(self, *args, **options):
        self.populate_profile(10002)
        print('profile done')
        self.populate_tag(10002)
        print('tag done')
        self.populate_question(100002)
        print('question done')
        self.populate_answer(1000002)
        print('answer done')
        self.populate_like_on_question(1000002)
        self.populate_like_on_answer(1000002)