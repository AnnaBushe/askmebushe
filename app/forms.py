from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Profile, User, Question, Answer


class LoginForm(forms.Form):
    login = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class RegistrationForm(UserCreationForm):
    avatar = forms.ImageField(required=False, initial=Profile._meta.get_field('avatar').get_default())

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'avatar']


class ProfileEditForm(UserChangeForm):
    avatar = forms.ImageField(required=False, initial=Profile._meta.get_field('avatar').get_default())

    def __init__(self, *args, **kwargs):
        super(ProfileEditForm, self).__init__(*args, **kwargs)
        del self.fields['password']

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'avatar']

    def save(self, *args, **kwargs):
        user = super().save(*args, **kwargs)
        user.profile.avatar = self.cleaned_data['avatar']
        user.profile.save()
        return user


class AddQuestionForm(forms.ModelForm):
    tags = forms.CharField()

    class Meta:
        model = Question
        fields = ['title', 'text']


class AddAnswerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddAnswerForm, self).__init__(*args, **kwargs)
        self.fields['text'].label = ''

    class Meta:
        model = Answer
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={'placeholder': 'Enter your answer here'})
        }