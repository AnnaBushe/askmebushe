from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator
from django.contrib import auth
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from datetime import datetime
from django.http import JsonResponse
from django.db.models import F

from .models import Question, Tag, Profile, ACTIONS, LikeOnQuestion, LikeOnAnswer
from .forms import LoginForm, RegistrationForm, ProfileEditForm, AddQuestionForm, AddAnswerForm


def get_paginator(request, objects, count):
    paginator = Paginator(objects, count)
    return paginator.get_page(request)


def base_popular():
    context = {'popular_tags': Tag.objects.popular(10),
               'best_members': Profile.objects.popular(10)
               }
    return context


def index(request):
    questions = Question.objects.new_questions()
    paginated_questions = get_paginator(request.GET.get('page'), questions, 10)

    context = base_popular()
    context.update({'page_name': 'AskMeBushe', 'questions': paginated_questions})

    return render(request, 'index.html', context)


def hot_questions(request):
    questions = Question.objects.hot_questions()
    paginated_questions = get_paginator(request.GET.get('page'), questions, 10)

    context = base_popular()
    context.update({'page_name': 'Hot questions', 'questions': paginated_questions})

    return render(request, 'index.html', context)


def login(request):
    context = base_popular()
    context.update({'page_name': 'Login'})
    if request.method == 'GET':
        continue_param = request.GET.get('continue')
        next_param = request.GET.get('next')
        if continue_param:
            context['continue'] = continue_param
        elif next_param:
            context['continue'] = next_param
        else:
            context['continue'] = 'main_page'
        form = LoginForm()
    else:
        form = LoginForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['login']
            password = form.cleaned_data['password']
            user = auth.authenticate(request, username=username, password=password)
            if user is not None:
                auth.login(request, user)
                return redirect(request.GET.get('continue'))
            else:
                form.add_error(None, 'User does not exist')
    context['form'] = form
    return render(request, 'registration/login.html', context)


def signup(request):
    context = base_popular()
    context.update({'page_name': 'Registration'})
    if request.method == 'GET':
        form = RegistrationForm()
    else:
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.avatar = form.cleaned_data['avatar']
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = auth.authenticate(username=user.username, password=raw_password)
            auth.login(request, user)
            return redirect('main_page')

    context['form'] = form
    return render(request, 'registration/signup.html', context)


def logout(request):
    auth.logout(request)
    return redirect(request.GET.get('current_page'))


@login_required(login_url='/login/?continue=/profile/edit/')
def settings(request):
    context = base_popular()
    context.update({'page_name': 'Settings'})

    user = request.user
    if request.method == 'GET':
        form = ProfileEditForm(instance=user,
                               initial={
                                    'username': user.username,
                                    'email': user.email,
                                    'first_name': user.first_name,
                                    'last_name': user.last_name,
                                    'avatar': user.profile.avatar
                               })
    else:
        form = ProfileEditForm(instance=user,
                               data=request.POST,
                               files=request.FILES)
        if form.is_valid():
            user = form.save()
            context['success'] = 'All changes saved'
            form = ProfileEditForm(instance=user,
                                   initial={
                                       'username': user.username,
                                       'email': user.email,
                                       'first_name': user.first_name,
                                       'last_name': user.last_name,
                                       'avatar': user.profile.avatar
                                   })

    context['form'] = form
    return render(request, 'profile/settings.html', context)


@login_required(login_url='/login/?continue=/ask/')
def ask(request):
    context = base_popular()
    context.update({'page_name': 'New question'})
    if request.method == 'GET':
        form = AddQuestionForm()
    else:
        form = AddQuestionForm(data=request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.user = request.user.profile
            question.save()

            tag_names = form.cleaned_data['tags'].split(', ')
            tag_names = [tag_name.strip() for tag_name in tag_names]

            for tag_name in tag_names:
                tag, created = Tag.objects.get_or_create(name=tag_name)
                question.tags.add(tag)

            return redirect('question_page', num=question.id)

    context['form'] = form
    return render(request, 'ask.html', context)


@login_required(login_url='/login/?continue=')
def send_answer(request, question):
    form = AddAnswerForm(data=request.POST)
    answer = form.save(commit=False)
    answer.question = question
    answer.user = request.user.profile
    answer.save()
    url = "{}#answer_{}".format(reverse('question_page', kwargs={'num': question.id}), str(answer.id))
    return redirect(url)


def one_question(request, num):
    context = base_popular()

    question = get_object_or_404(Question, pk=num)
    context.update({'page_name': 'Question', 'question': question})

    if request.method == 'GET':
        form = AddAnswerForm()
    else:
        return send_answer(request, question)

    context['form'] = form
    return render(request, 'question.html', context)


def tag_search(request, tag_name):
    tag = get_object_or_404(Tag, name=tag_name)
    questions = Question.objects.by_tag(tag)
    paginated_questions = get_paginator(request.GET.get('page'), questions, 10)

    context = base_popular()
    context.update({'page_name': tag_name, 'tag_name': tag_name, 'questions': paginated_questions})

    return render(request, 'tags.html', context)


@login_required(login_url='/login/?continue=')
@require_POST
def vote(request):
    data = request.POST
    qid = data['qid']
    action = data['action']
    inc = ACTIONS[action]
    question = get_object_or_404(Question, id=qid)
    new_vote = LikeOnQuestion(user=request.user.profile,
                              question_id=question.id,
                              attitude=inc)
    new_vote.save()
    question.rating = F('rating') + inc
    print('hey')
    question.save()
    return JsonResponse(data)
