from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count, Sum, Value
from django.db.models.signals import post_save
from django.dispatch import receiver


class QuestionManager(models.Manager):
    def new_questions(self):
        return self.select_related('user').all().order_by('-date')

    def hot_questions(self):
        return self.select_related('user').all().order_by('-rating')

    def by_tag(self, tag):
        return self.select_related('user').filter(tags=tag).order_by('-date')


class ProfileManager(models.Manager):
    def popular(self, records_num):
        return self.annotate(rating=Sum('answers__rating')).order_by('-rating')[:records_num]


default_image_path = 'default_profile_img.jpg'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='avatar/%Y%m%d', default=default_image_path)
    objects = ProfileManager()

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class TagManager(models.Manager):
    def popular(self, records_num):
        return self.annotate(count=Count('questions')).order_by('-count')[:records_num]


class Tag(models.Model):
    name = models.CharField(max_length=255, unique=True)
    objects = TagManager()

    def __str__(self):
        return self.name


class Question(models.Model):
    title = models.CharField(max_length=512)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    tags = models.ManyToManyField(Tag, related_name='questions')
    rating = models.IntegerField(default=0)

    objects = QuestionManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'


class Answer(models.Model):
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True, related_name='answers')
    correct = models.BooleanField(default=False)
    rating = models.IntegerField(default=0)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'


LIKE = 1
DISLIKE = -1
CHOICES = ((LIKE, 'like'), (DISLIKE, 'dislike'))
ACTIONS = {x[1]: x[0] for x in CHOICES}


class LikeOnQuestion(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    attitude = models.IntegerField(choices=CHOICES)

    class Meta:
        unique_together = ('user', 'question')


class LikeOnAnswer(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    attitude = models.IntegerField(choices=CHOICES)

    class Meta:
        unique_together = ('user', 'answer')

